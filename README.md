# fuse-infra

Tikal FuseDay 2020 Infrastructure

## Introduction
The purpose of this project is to provide a basic AWS in-cluster infrastructure to be deployed on each team's cluster.
Infrastructure is designed to be deployed as an ArgoCD application, and assembled of the following two main component:
* `infra`
basic services (side-cars) to support other services development and deployment:
  * cert-manager
  * external-dns
  * external-secrets
  * nginx-ingress
  
* `whoami`
sample application to demonstrate how to use infra's side-cars, includes manifests:
  * deployment
  * service
  * ingress (TLS termination)

## Getting Started
This solution is assembled of the following three layers

### Overlays
Once subjected cluster is up and ready to accept workload, go over to [overlays](overlays) directory and select your desired cluster sub-directory.  
Each cluster's sub-directory reflects cluster specific implementation, see both `../infra/infra.yml` and `../whoami/whoami.yml`.

### Bases
Bases layer defines each component base configuration which is inherited or overrides by each cluster's implementation in `overlays`.
>WARNING: any change to these configurations can affect all clusters implementations

### Apps
Apps layer controls to look-and-feel in ArgoCD and which applications are to be deployed for each cluster.
Like `overlays` it's also separated by specific cluster sub-directory.

#### Provision
ArgoCD application provision is simple as applying any other manifest, and requires `kubectl` and `kustomize`.

Select your desired cluster (cluster1 in this example) and follow the steps below:
```
export KUBECONFIG=cluster1.kubeconfig
kustomize build apps/cluster1 | kubectl apply -f -
```

The results will be two un-synced ArgoCD application.

#### Sync
Use ArgoCD to sync application.  